<?php

declare(strict_types=1);

namespace WdevsCustomBar\NotifyCustomer\Model\Config;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Notify
 * @package WdevsCustomBar\NotifyCustomer\Model\Config
 */
class Notify
{
    const NOTIFY_ENABLE = 'notify/configurable/enabled';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Shipping constructor.
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig
    ) {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * enable notify customer module.
     * @return mixed
     */
    public function isNotifyEnabled()
    {
        return $this->scopeConfig->getValue(self::NOTIFY_ENABLE);
    }
}

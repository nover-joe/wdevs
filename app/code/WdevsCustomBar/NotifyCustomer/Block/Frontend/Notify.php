<?php

namespace WdevsCustomBar\NotifyCustomer\Block\Frontend;

/**
 * Html page notify block
 */
class Notify extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Customer\Api\GroupRepositoryInterface
     */
    protected $_groupRepository;

    /**
     * @var \WdevsCustomBar\NotifyCustomer\Model\Config\Notify
     */
    protected $_notifyConfig;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Api\GroupRepositoryInterface $groupRepository
     * @param \WdevsCustomBar\NotifyCustomer\Model\Config\Notify $notifyConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Api\GroupRepositoryInterface $groupRepository,
        \WdevsCustomBar\NotifyCustomer\Model\Config\Notify $notifyConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_customerSession = $customerSession;
        $this->_groupRepository = $groupRepository;
        $this->_notifyConfig = $notifyConfig;
        $this->_isScopePrivate = true;
    }

    /**
     * show notify by customer group
     * @return string
     */
    public function NotifyCustomer()
    {
        if ($this->isLogged() && $this->_notifyConfig->isNotifyEnabled()) {
            $customerGroupId = $this->_customerSession->getCustomer()->getGroupId();
            return $this->NotifyContent($customerGroupId);
        }
        return 'This is the content for: Guests';
    }

    public function NotifyContent($customerGroupId)
    {
        if ($customerGroupId === null) {
            return 'This is the content for: Guests';
        }

        switch ($customerGroupId) {
            case 0:
            case 1:
                $notifyContent = 'This is the content for: ' . $this->getGroupName($customerGroupId);
                break;
            default:
                $notifyContent = 'This is the content for: General Customers';
                break;
        }
        return $notifyContent;
    }

    /**
     * Check customer is logged.
     *
     * @return boolean
     */
    public function isLogged()
    {
        return $this->_customerSession->isLoggedIn();
    }

    /**
     * get customer Group name.
     * @param $groupId
     */
    public function getGroupName($groupId)
    {
        $group = $this->_groupRepository->getById($groupId);
        return $group->getCode();
    }
}
